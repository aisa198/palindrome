﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Palindrome
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine(IsPalindrome(Console.ReadLine()));
            Console.ReadKey();
        }

        public static string IsPalindrome(string inputToCheck)
        {
            var start = 0;
            var end = inputToCheck.Length - 1;
            while (true)
            {
                if (start > end)
                {
                    return "YES";
                }
                char a = inputToCheck[start];
                char b = inputToCheck[end];
                if (!Char.IsLetter(a))
                {
                    start++;
                }
                else if (!Char.IsLetter(b))
                {
                    end--;
                }
                else if(char.ToLower(a) != char.ToLower(b))
                {
                    return "NO";
                }
                else
                {
                    start++;
                    end--;
                }
            }
        }
    }
}
