﻿using NUnit.Framework;

namespace Palindrome
{
    [TestFixture]
    class ProgramTests
    {
        [Test]
        public void IsPalindrome_emptyString_YES()
        {
            //arrange
            var input = "";
            //act
            var result = Program.IsPalindrome(input);
            //assert
            Assert.AreEqual("YES", result);
        }
        [Test]
        public void IsPalindrome_inputWithoutLetters_YES()
        {
            //arrange
            var input = "123_(*&^%";
            //act
            var result = Program.IsPalindrome(input);
            //assert
            Assert.AreEqual("YES", result);
        }
        [Test]
        public void IsPalindrome_truePalindrome_YES()
        {
            //arrange
            var input1 = "Able was I ere I saw Elba";
            var input2 = "A man, a plan, a canal – Panama";
            var input3 = "redivider";
            var input4 = "abut-1-tuba";
            var input5 = "A";
            //act
            var result1 = Program.IsPalindrome(input1);
            var result2 = Program.IsPalindrome(input2);
            var result3 = Program.IsPalindrome(input3);
            var result4 = Program.IsPalindrome(input4);
            var result5 = Program.IsPalindrome(input5);
            //assert
            Assert.AreEqual("YES", result1);
            Assert.AreEqual("YES", result2);
            Assert.AreEqual("YES", result3);
            Assert.AreEqual("YES", result4);
            Assert.AreEqual("YES", result5);
        }
        [Test]
        public void IsPalindrome_falsePalindrome_YES()
        {
            //arrange
            var input1 = "Able was I sEE Elba";
            var input2 = "A man, a plan, a canal – Panamaa";
            var input3 = "!@redividersity";
            var input4 = "tyY7";
            var input5 = "@allula ";
            //act
            var result1 = Program.IsPalindrome(input1);
            var result2 = Program.IsPalindrome(input2);
            var result3 = Program.IsPalindrome(input3);
            var result4 = Program.IsPalindrome(input4);
            var result5 = Program.IsPalindrome(input5);
            //assert
            Assert.AreEqual("NO", result1);
            Assert.AreEqual("NO", result2);
            Assert.AreEqual("NO", result3);
            Assert.AreEqual("NO", result4);
            Assert.AreEqual("NO", result5);
        }
    }
}
